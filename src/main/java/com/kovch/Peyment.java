package com.kovch;


import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class Peyment {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double payments = scanner.nextDouble();
        scanner.close();

        NumberFormat us = NumberFormat.getCurrencyInstance(Locale.US);
        Locale indiaLocal;
        indiaLocal = new Locale("En","IN");
        NumberFormat india = NumberFormat.getCurrencyInstance(indiaLocal);
        NumberFormat china = NumberFormat.getCurrencyInstance(Locale.CHINA);
        NumberFormat france = NumberFormat.getCurrencyInstance(Locale.FRANCE);
        System.out.println("US: " + us.format(payments));
        System.out.println("India: " + india.format(payments));
        System.out.println("China: " + china.format(payments));
        System.out.println("France: " + france.format(payments));
    }

}
