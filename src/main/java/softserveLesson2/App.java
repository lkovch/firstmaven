package softserveLesson2;

//Add class Employee to the project.
//Class Employee should consists of
//	three private fields: name, rate and hours;
//	static field totalSum
//	properties for access to these fields;
//	default constructor, constructor with 2 parameters (name and rate) and constructor with 3 parameters;
//	methods:
//salary - to calculate the salary of person (rate * hours)
//toString - to output information about employee
//changeRate - to change the rate of employee
//bonuses – to calculate 10% from salary
//In the method main() create 3 objects of Employee type. Input information about them.
//Display the total salary of all workers to screen

class Employee{
    private String name;
    private double rate;
    private int hours;
    static double totalSum;

    public Employee(){
    }
    public Employee(String name, double rate){
        this.name = name;
        this.rate = rate;
    }
    public Employee(String name, double rate, int hours){
        this.name = name;
        this.rate = rate;
        this.hours = hours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }
    public double getSalary(){
        return rate*hours;
    }
    public double getBonuses(){
        return getSalary()*0.1;
    }
    public double getTotalSum(){
        totalSum = getSalary()+getBonuses();
        return totalSum;
    }
    public void getChangeRate(double rate) { this.rate=rate; }
    @Override
    public String toString(){
        return "Employee [name " + name + ", rate " + rate + ", hours " + hours +"]";
    }

}

public class App {
    public static void main(String[] args) {
        Employee em1 = new Employee("Smith", 7);
        Employee em2 = new Employee("Robinson", 9, 96);
        Employee em3 = new Employee("Shevchenko", 13, 89);
        System.out.println(em1);
        System.out.println(em2);
        System.out.println(em3);
        System.out.println("Salary of employee " + em1.getName() + " is " + em1.getSalary() + ", he has a bonus " + em1.getBonuses());
        System.out.println(em1.getName() + " has earn " + em1.getTotalSum());
        System.out.println("Salary of employee " + em2.getName() + " is " + em2.getSalary() + ", he has a bonus " + em2.getBonuses());
        System.out.println(em2.getName() + " has earn " + em2.getTotalSum());
        System.out.println("Salary of employee " + em3.getName() + " is " + em3.getSalary() + ", he has a bonus " + em3.getBonuses());
        System.out.println(em3.getName() + " has earn " + em3.getTotalSum());
        em2.getChangeRate(14);
        System.out.println();
        System.out.println("New salary of employee " + em2.getName() + " is " + em2.getSalary() + ", he has a new bonus " + em2.getBonuses());
        System.out.println(em2.getName() + " has earn " + em2.getTotalSum());
    }
}