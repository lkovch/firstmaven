package softserveLesson2;

import java.util.Calendar;

//Add class Person to the project.
//Class Person should consists of
//two private fields: name and birthYear (the birthday year)
//properties for access to these fields
//default constructor and constructor with 2 parameters
//methods:
//age - to calculate the age of person
//input - to input information about person
//output - to output information about person
//changeName - to change the name of person
//In the method main() create 5 objects of Person type and input information about them.

class Person{
    private String name;
    private int birthYear;

    public Person() {
    }

    public Person(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }

    public static int presentYear()
    {
        int year = Calendar.getInstance().get(Calendar.YEAR);
        return year;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }
    public int age() {
        return (presentYear() - birthYear);
    }
    public void input(String name, int birthYear) {
        this.name = name;
        this.birthYear = birthYear;
    }
    public void output() {
        System.out.println("Neme: " + name + ". Age " + age());
    }
    public void changeName(String name) {
        this.name = name;
    }
}
public class AppPerson {
    public static void main(String[] args) {
        Person person1 = new Person();
        Person person2 = new Person();
        Person person3 = new Person("Bob", 1980);
        Person person4 = new Person("Julia", 1990);
        Person person5 = new Person("Olga", 1985);
        person1.input("Petro", 1972);
        person2.input("Alex",1982);
        person1.output();
        person2.output();
        person3.output();
        person4.output();
        person5.output();
        person2.changeName("Alex Smith");
        person2.output();
    }

}
