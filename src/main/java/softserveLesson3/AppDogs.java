package softserveLesson3;
//Сreate class Dog with fields name, breed, age.
//Create 3 instances of type Dog.
//Check if there is no two dogs with the same name.
//Display the name and the kind of the oldest dog.
class Dog{
    private String name;
    private String breed;
    private int age;

    public Dog() {
    }
    public Dog(String name, String breed, int age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getBreed() {
        return breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
}
public class AppDogs {
    public static void main(String[] args) {

        Dog dog1 = new Dog("Bob", "Shipdog", 8);
        Dog dog2 = new Dog("Merci", "Collie", 1);
        Dog dog3 = new Dog("Jack", "Terrier", 12);
        if (dog1.getName() == dog2.getName() || dog1.getName() == dog3.getName() || dog2.getName() == dog3.getName()) {
            System.out.println("We have dogs with the same name");
        } else {
            System.out.println("We don't have dogs with the same name");
        }

        if (dog1.getAge() >= dog2.getAge() && dog1.getAge() >= dog3.getAge()) {
            System.out.println("The oldest dog is " + dog1.getName() + ", breed " + dog1.getBreed());
        }
        if (dog2.getAge() >= dog1.getAge() && dog2.getAge() >= dog3.getAge()) {
            System.out.println("The oldest dog is " + dog2.getName() + ", breed " + dog2.getBreed());
        }
        if (dog3.getAge() >= dog1.getAge() && dog3.getAge() >= dog2.getAge()) {
            System.out.println("The oldest dog is " + dog3.getName() + ", breed " + dog3.getBreed());
        }
    }
}
