package softserveLesson4;

import java.util.Scanner;

//Create a class Employee with fields name, department number, salary.
//Create five objects of class Employee.
//Display all employees of a certain department (enter department number in the console);
//arrange workers by the field salary in descending order.
class Empoyee{
    private String name;
    private int department;
    private int salary;

    public Empoyee(){
    }
    public Empoyee(String name,int department,int salary){
        this.name=name;
        this.department=department;
        this.salary=salary;
    }
    public String getName(){
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getDepartment() {
        return department;
    }
    public void setDepartment(int department) {
        this.department = department;
    }

    public int getSalary() {
        return salary;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void output (){
        System.out.println("Employee " + name + ", department " + department + ", salary " + salary);
    }
}
public class AppEmployee {
    public static void main(String[] args) {
        Empoyee [] empoyee = new Empoyee[5];
        empoyee[0]= new Empoyee("Ivanenko", 101, 15000);
        empoyee[1]= new Empoyee("Petrenko", 101, 12000);
        empoyee[2]= new Empoyee("Franko", 102,18000);
        empoyee[3]= new Empoyee("Antonov", 103, 8000);
        empoyee[4]= new Empoyee("Shevchenko",102, 25000);

        System.out.println("Enter the number of department 101, 102 or 103:");
        Scanner sc = new Scanner(System.in);
        int departmentNumber = sc.nextInt();
        while(departmentNumber!=101 && departmentNumber!=102 && departmentNumber!=103 ){
            System.out.println("You wrote the wrong number, please try again:");
            departmentNumber = sc.nextInt();
            }
        if(departmentNumber==101 || departmentNumber==102 || departmentNumber==103 ) {
            for (int i = 0; i < empoyee.length; i++) {
                if (empoyee[i].getDepartment() == departmentNumber) {
                    empoyee[i].output();
                }
            }
        }
        //arrange workers by the field salary in descending order
        System.out.println();
        System.out.println("Salary in descending order:");

        Empoyee tmp = new Empoyee();
        for(int i =0; i<empoyee.length-1; i++){
            for(int j =i+1; j<empoyee.length; j++){
                if(empoyee[i].getSalary()<empoyee[j].getSalary()){
                    tmp=empoyee[i];
                    empoyee[i]=empoyee[j];
                    empoyee[j]=tmp;
                }
            }
        }
        for (int i=0; i<empoyee.length; i++){
            empoyee[i].output();
        }
    }
}

