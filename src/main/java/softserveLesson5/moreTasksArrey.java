package softserveLesson5;

import java.util.Scanner;

//Enter 10 integer numbers.
// Calculate the sum of first 5 elements if they are positive
// or product of last 5 element in the other case.
// Find position of second positive number
// minimum and its position in the array
public class moreTasksArrey {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numbers []= new int[10];
        for(int i=0; i<numbers.length; i++){
            numbers[i]=sc.nextInt();
        }
        //Calculate the sum of first 5 elements if they are positive;
        int sum =0;
        int product =1;
        boolean flag = true;
        for (int i=0; i<5; i++){
            if(numbers[i]<0){
                flag = false;
            }
        }
        if(flag==true){
            for(int i=0; i<5; i++){
                sum+=numbers[i];
            }
            System.out.println("Sum of the firrst 5 elements = " +sum);
        }
        //Calculate the product of last 5 element if the first 5 elements aren't positive
        else if (flag==false){
            for (int i=numbers.length-5; i<numbers.length; i++){
                product*=numbers[i];
            }
            System.out.println("The product of the last 5 elements = " +product);
        }
        //Find position of second positive number
        int pspn=0; //position of second positive number
        int count =0;
        for (int i=0; i<numbers.length; i++){
            if (numbers[i]>0){
                count++;
            }if(count==2){
                pspn = i+1;
                break;
            }
        }
        System.out.println("position of second positive number is " + pspn);
        //Find minimum and its position in the array
        int minimumNumber = 0;
        int pmn =0; //position of minimum number
        for (int i=0; i<numbers.length; i++){
            if(numbers[i]<minimumNumber){
                minimumNumber=numbers[i];
                pmn = i+1;
            }
        }
        System.out.println("minimam number is " +minimumNumber + ",and his position " +pmn);
    }
}
