package softserveLesson5;
//Create an array of ten integers. Display the biggest of these numbers;
//the sum of positive numbers in the array; the amount of negative numbers in the array.
//What values there are more: negative or positive?
public class tasksArrey {
    public static void main(String[] args) {
        int numbers [] = {4, 5, -10, 24, 0, -6, 7, -2, 22, 26};
        int biggestNunber = numbers [0];
        //found out the biggest number
        for (int i =0; i<numbers.length; i++){
            if (numbers[i]>biggestNunber){
                biggestNunber = numbers [i];
            }
        }
        System.out.println("The biggest number is " + biggestNunber);
        //found out the sum of all positive numbers
        int positiveSum = 0;
        for (int i=0; i<numbers.length; i++){
            if(numbers[i]>0){
                positiveSum = positiveSum+numbers[i];
                continue;
            }
        }
        System.out.println("Sum of positive numbers = " + positiveSum);

        //found out the amount of negative numbers
        int amountOfNegativeNumbers = 0;
        for (int i=0; i<numbers.length; i++){
            if (numbers[i]<0){
                amountOfNegativeNumbers++;
            }
        }
        System.out.println("We have " + amountOfNegativeNumbers + " negative numbers.");

        //found out which numbers are more. Negative or positive.
        if(amountOfNegativeNumbers==numbers.length/2){
            System.out.println("We have the same count of negative and positive numbers");
        }else if(amountOfNegativeNumbers>numbers.length/2){
            System.out.println("We have more negative numbers than positive");
        }else
            System.out.println("We have more positive numbers");

    }
}
